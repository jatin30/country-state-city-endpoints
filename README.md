# country-state-city endpoints

Filter raw data from country-state-city endpoints

## Getting started


./data - It contains initial raw data (coutries.json, states.json, cities.json).

./out - It contains filtered data.

To generate filtered data, run the following command:

```
    go run generate.go
```
Then start the project using the following command:

```
    go run main.go
```


### Endpoints

```
countries: /countries
states: /states/{country_id}
cities: /cities/{state_id}
```


#### Raw data Source: https://data.world/dr5hn/country-state-city