package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"main/generate"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

func handleRequests() {
	port := os.Getenv("PORT")
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/countries", getCountries).Methods("GET")
	myRouter.HandleFunc("/states/{country_id}", getStates).Methods("GET")
	myRouter.HandleFunc("/cities/{state_id}", getCities).Methods("GET")

	http.ListenAndServe(":"+port, myRouter)
}

func main() {
	err := godotenv.Load("./.env")
	if err != nil {
		fmt.Println("Error loading .env file")
	}
	generate.Generate()
	handleRequests()
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Server is up and running")
	fmt.Println("Endpoint Hit: homePage")
}

func getCountries(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	file, err := ioutil.ReadFile("./out/countries.json")
	if err != nil {
		fmt.Fprintln(w, err)
		fmt.Fprintln(w, "try generating json files using 'go run generate.go'")
		return
	}

	w.Write(file)
}

func getStates(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	countryId := params["country_id"]

	file, err := ioutil.ReadFile("./out/states/" + countryId + ".json")
	if err != nil {
		fmt.Fprintln(w, err)
		fmt.Fprintln(w, "try generating json files using 'go run generate.go'")
		return
	}

	w.Write(file)
}

func getCities(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	stateId := params["state_id"]

	file, err := ioutil.ReadFile("./out/cities/" + stateId + ".json")
	if err != nil {
		fmt.Fprintln(w, err)
		fmt.Fprintln(w, "try generating json files using 'go run generate.go'")
		return
	}

	w.Write(file)
}
