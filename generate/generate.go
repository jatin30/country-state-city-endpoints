package generate

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"io/ioutil"
	"os"
)

type CountryObj struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
	Iso  string `json:"iso"`
}

type StateObj struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	Code      string `json:"code"`
	CountryId int    `json:"country_id"`
}

type CityObj struct {
	Id      int    `json:"id"`
	Name    string `json:"name"`
	StateId int    `json:"state_id"`
}

type CountryUUIDObj struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Iso  string `json:"iso"`
}

type StateUUIDObj struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	Code      string `json:"code"`
	CountryId string `json:"country_id"`
}

type CityUUIDObj struct {
	Id      string `json:"id"`
	Name    string `json:"name"`
	StateId string `json:"state_id"`
}

func getAllCountries() <-chan []CountryObj {
	res := make(chan []CountryObj)

	go func() {
		defer close(res)

		var countries []CountryObj
		file, err := ioutil.ReadFile("./data/countries.json")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		json.Unmarshal(file, &countries)

		res <- countries
	}()

	return res
}

func getAllStates() <-chan []StateObj {
	res := make(chan []StateObj)

	go func() {
		defer close(res)

		var states []StateObj
		file, err := ioutil.ReadFile("./data/states.json")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		json.Unmarshal(file, &states)

		res <- states
	}()

	return res
}

func getAllCities() <-chan []CityObj {
	res := make(chan []CityObj)

	go func() {
		defer close(res)

		var cities []CityObj
		file, err := ioutil.ReadFile("./data/cities.json")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		json.Unmarshal(file, &cities)

		res <- cities
	}()

	return res
}

func Generate() {
	countriesCh, statesCh, citiesCh := getAllCountries(), getAllStates(), getAllCities()
	countries, states, cities := <-countriesCh, <-statesCh, <-citiesCh

	_ = os.RemoveAll("./out/states")
	os.MkdirAll("./out/states", 0755)
	_ = os.RemoveAll("./out/cities")
	os.MkdirAll("./out/cities", 0755)

	updatedCountries := make([]CountryUUIDObj, 0)
	for _, country := range countries {
		countryUuid := uuid.New()
		countryUuidObj := CountryUUIDObj{
			Id:   countryUuid.String(),
			Name: country.Name,
			Iso:  country.Iso,
		}
		fmt.Println(countryUuidObj.Id, countryUuidObj.Name)
		updatedCountries = append(updatedCountries, countryUuidObj)

		filteredStates := filterStates(states, country.Id)
		updatedStates := make([]StateUUIDObj, 0)
		for _, state := range filteredStates {
			stateUuid := uuid.New()
			stateUuidObj := StateUUIDObj{
				Id:        stateUuid.String(),
				Name:      state.Name,
				Code:      state.Code,
				CountryId: countryUuid.String(),
			}
			//fmt.Println("\t", stateUuidObj.Id, stateUuidObj.Name, stateUuidObj.CountryId)
			updatedStates = append(updatedStates, stateUuidObj)

			filteredCities := filterCities(cities, state.Id)
			updatedCities := make([]CityUUIDObj, 0)
			for _, city := range filteredCities {
				cityUuid := uuid.New()
				cityUuidObj := CityUUIDObj{
					Id:      cityUuid.String(),
					Name:    city.Name,
					StateId: stateUuid.String(),
				}
				//fmt.Println("\t\t", cityUuidObj.Id, cityUuidObj.Name, cityUuidObj.StateId)
				updatedCities = append(updatedCities, cityUuidObj)
			}

			outCityFile, _ := json.MarshalIndent(updatedCities, "", "  ")
			_ = ioutil.WriteFile("./out/cities/"+stateUuidObj.Id+".json", outCityFile, 0644)

		}

		outStateFile, _ := json.MarshalIndent(updatedStates, "", "  ")
		_ = ioutil.WriteFile("./out/states/"+countryUuidObj.Id+".json", outStateFile, 0644)

	}

	outCountryFile, _ := json.MarshalIndent(updatedCountries, "", "  ")
	_ = ioutil.WriteFile("./out/countries.json", outCountryFile, 0644)

}

func filterStates(states []StateObj, countryId int) []StateObj {
	var filteredStates []StateObj
	for _, state := range states {
		if state.CountryId == countryId {
			filteredStates = append(filteredStates, state)
		}
	}
	return filteredStates
}

func filterCities(cities []CityObj, stateId int) []CityObj {
	var filteredCities []CityObj
	for _, city := range cities {
		if city.StateId == stateId {
			filteredCities = append(filteredCities, city)
		}
	}
	return filteredCities
}
