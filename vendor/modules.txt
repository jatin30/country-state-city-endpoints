# github.com/google/uuid v1.3.0
## explicit
github.com/google/uuid
# github.com/gorilla/mux v1.8.0
## explicit; go 1.12
github.com/gorilla/mux
# github.com/joho/godotenv v1.4.0
## explicit; go 1.12
github.com/joho/godotenv
